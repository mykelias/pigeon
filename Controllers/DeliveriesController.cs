﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pigeon.Models;
using Pigeon.Repository;

namespace Pigeon.Controllers
{
    /// <summary>
    /// Deliveries endpoint of Pideon API.
    /// </summary>
    [ApiConventionType(typeof(DefaultApiConventions))]
    [ApiController]
    [Authorize]
    [Route("api/deliveries")]
    public class DeliveriesController : ControllerBase
    {
        const string addOneConst = "isso eh uma constante";
        private readonly IDeliveriesRepository _deliveriesRepository;
        private readonly ILogger<DeliveriesController> _logger;

        /// <summary>
        /// Creates a new instance of <see cref="DeliveriesController"/> with dependencies injected.
        /// </summary>
        /// <param name="deliveriesRepository">A repository for managing the deliveries.</param>
        /// <param name="logger">Logger implementation.</param>
        public DeliveriesController(IDeliveriesRepository deliveriesRepository, ILogger<DeliveriesController> logger)
        {
            _deliveriesRepository = deliveriesRepository;
            _logger = logger;
        }

        /// <summary>
        /// Get one page of deliveries.
        /// </summary>
        /// <param name="page">Page number.</param>
        /// <param name="size">Page size.</param>
        /// <remarks>If you omit <c>page</c> and <c>size</c> query parameters, you'll get the first page with 10 deliveries.</remarks>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(PagedList<Delivery>))]        
        public ActionResult<PagedList<Delivery>> GetAll(int page = 1, int size = 10)
        {   
            _logger.LogDebug("Getting one page of deliveries");

            var deliveries = _deliveriesRepository.GetPage(page, size);
            
            //add custon header in response
            Request.HttpContext.Response.Headers.Add("Total-Count", deliveries.TotalCount.ToString());
            
            return deliveries;
        }

        /// <summary>
        /// Get a single delivery by id.
        /// </summary>
        /// <param name="id">Id of the delivery to retrieve.</param>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Delivery))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Delivery> GetById(string id)
        {
            _logger.LogDebug($"Getting a delivery with id {id}");

            if (string.IsNullOrWhiteSpace(id))
                return NotFound();

            var delivery = _deliveriesRepository.GetById(id);

            if (delivery == null)
                return NotFound();

            return delivery;
        }

        /// <summary>
        /// Create a new delivery from the supplied data.
        /// </summary>
        /// <param name="model">Data to create the delivery from.</param>
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Delivery))]
        public ActionResult<Delivery> Post(DeliveryDto model)
        {
            _logger.LogDebug($"Creating a new delivery with code \"{model.Code}\"");
            var delivery = new Delivery();
            model.MapToDelivery(delivery);

            _deliveriesRepository.Create(delivery);

            return CreatedAtAction(nameof(GetById), "deliveries", new { id = delivery.Id }, delivery);
        }

        /// <summary>
        /// Updates the delivery with the given id.
        /// </summary>
        /// <param name="id">Id of the delivery to update.</param>
        /// <param name="model">Data to update the delivery from.</param>
        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Delivery))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Put(string id, DeliveryDto model)
        {
            _logger.LogDebug($"Updating a delivery with id {id}");

            var delivery = _deliveriesRepository.GetById(id);

            if (delivery == null)
                return NotFound();

            model.MapToDelivery(delivery);

            _deliveriesRepository.Update(delivery);

            return Ok(delivery);
        }

        /// <summary>
        /// Delete the delivery with the given id.
        /// </summary>
        /// <param name="id">Id of the delivery to delete.</param>
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            _logger.LogDebug($"Deleting delivery with id {id}");

            if (string.IsNullOrWhiteSpace(id))
                return NotFound();

            var delivery = _deliveriesRepository.GetById(id);
            if (delivery == null)
                return NotFound();

            _deliveriesRepository.Delete(id);

            return Ok();
        }
    }
}
