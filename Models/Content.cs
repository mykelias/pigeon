using Pigeon.Models.Enums;

namespace Pigeon.Models
{
    public class Content
    {
        /// <summary>
        /// Gets or sets the kind.
        /// </summary>
        public EContent Kind { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public decimal Value { get; set; }
    }
}