namespace Pigeon.Models
{
    /// <summary>
    /// Represents a single delivery.
    /// </summary>
    public class Delivery
    {
        /// <summary>
        /// Gets or sets the delvery id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets the recipient.
        /// </summary>
        public string Recipient { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public Content Content { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public decimal Value { get; set; }
    }
}