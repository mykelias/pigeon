using System.ComponentModel.DataAnnotations;

namespace Pigeon.Models
{
    public class DeliveryDto
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the release sender.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets the release recipient.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Recipient { get; set; }

        /// <summary>
        /// Gets or sets the release content.
        /// </summary>
        public Content Content { get; set; }

        /// <summary>
        /// Gets or sets the release value.
        /// </summary>
        [Required]
        public decimal Value { get; set; }

        /// <summary>
        /// Copy data to <see cref="Delivery"/> instance.
        /// </summary>
        /// <param name="delivery">Delivery to copy the input data to.</param>
        public void MapToDelivery(Delivery delivery)
        {
            delivery.Code = Code;
            delivery.Sender = Sender;
            delivery.Recipient = Recipient;
            delivery.Value = Value;
            delivery.Content = Content;
        }
    }
}