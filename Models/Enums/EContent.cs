namespace Pigeon.Models.Enums
{
    public enum EContent
    {
        normal,
        fragile,
        big,
        small
    }
}