using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pigeon.Models;

namespace Pigeon.Repository
{
    public class DeliveriesRepository : IDeliveriesRepository
    {
        private IList<Delivery> _deliveries;

        public PagedList<Delivery> GetPage(int page = 1, int pageSize = 10)
        {
            var deliveries = GetDeliveries();

            return new PagedList<Delivery>
            {
                Items = deliveries.Skip((page - 1) * pageSize).Take(pageSize),
                Page = page,
                PageSize = pageSize,
                TotalCount = deliveries.Count
            };
        }

        public Delivery GetById(string id)
        {
            var delivery = GetDeliveries();
            return delivery.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Delivery delivery)
        {
            delivery.Id = GetNextId();
            GetDeliveries().Add(delivery);
        }

        public void Delete(string id)
        {
            var delivery = GetDeliveries();
            var deliveryToDelete = delivery.First(x => x.Id == id);

            delivery.Remove(deliveryToDelete);
        }

        public void Update(Delivery delivery)
        {
            var deliveries = GetDeliveries();
            var deliveryToUpdate = deliveries.First(x => x.Id == delivery.Id);

            deliveries.Remove(deliveryToUpdate);
            deliveries.Add(delivery);
        }
        private IList<Delivery> GetDeliveries()
        {
            if (_deliveries == null)
            {
                var assembly = Assembly.GetEntryAssembly();
                var resourceStream = assembly.GetManifestResourceStream("Pigeon.Repository.deliveries.json");

                using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
                using (var jsonReader = new JsonTextReader(reader))
                {
                    var deliveriesArray = JObject.Load(jsonReader)["deliveries"];
                    _deliveries = deliveriesArray.ToObject<IList<Delivery>>();
                }
            }

            return _deliveries;
        }

        private string GetNextId()
        {
            var maxId = GetDeliveries().Select(x => int.Parse(x.Id.Split("-")[1])).Max();
            return $"deli-{maxId + 1:D6}";
        }
    }
}