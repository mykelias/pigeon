using System.Collections.Generic;
using Pigeon.Models;

namespace Pigeon.Repository
{
    public interface IDeliveriesRepository
    {
        PagedList<Delivery> GetPage(int page = 1, int pageSize = 10);
        Delivery GetById(string id);
        void Create(Delivery delivery);
        void Delete(string id);
        void Update(Delivery delivery);
    }
}