using Pigeon.Models;
using Pigeon.Repository;

namespace Pigeon.Test.UnitTests
{
    public class FakeDeliveriesRepository : IDeliveriesRepository
    {
        public PagedList<Delivery> GetPage(int page = 1, int pageSize = 10)
        {
            return new PagedList<Delivery>();
        }

        public Delivery GetById(string id)
        {
            return id == "existing" ? new Delivery { Id = id } : null;
        }

        public void Create(Delivery delivery)
        {
        }

        public void Delete(string id)
        {
        }

        public void Update(Delivery delivery)
        {
        }
    }
}